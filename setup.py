#!/usr/bin/env python3


# This file is part of pibot.

# pibot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pibot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pibot.  If not, see <https://www.gnu.org/licenses/>.


from setuptools import setup


def readme():
    with open("README.md", encoding="utf-8") as f:
        return f.read()


setup(
    name="pibot",
    version="0.0.0",
    description="Python IRC bot",
    author="XGQT",
    author_email="xgqt@riseup.net",
    url="https://gitlab.com/xgqt/pibot",
    long_description=readme(),
    long_description_content_type="text/markdown",
    license="GPL-3",
    keywords="irc",
    python_requires=">=3.6.*",
    packages=["pibot"],
    include_package_data=True,
    zip_safe=False,
    entry_points={
        "console_scripts": [
            "pibot = pibot.app:main"
        ],
    },
    classifiers=[
        "Development Status :: Beta",
        "Operating System :: POSIX",
        "Programming Language :: Python",
        "Topic :: Communications :: Email",
        "Topic :: Software Development :: Bug Tracking",
    ]
)
