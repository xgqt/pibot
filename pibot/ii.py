#!/usr/bin/env python3


# This file is part of pibot.

# pibot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pibot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pibot.  If not, see <https://www.gnu.org/licenses/>.


# IRC interaction class


import socket
import time


class IRC:

    irc = socket.socket()

    def __init__(self):

        # Define the socket
        self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def send(self, channel, message):

        print("[S] Sending {} to: {}".format(message, channel))
        self.irc.send(bytes(
            "PRIVMSG {} {}\n".format(channel, message), "UTF-8"
        ))

    def connect(self, server, port, channel, nick, password):

        # Connect to the server
        print("[C] Connecting to: {}".format(server))
        self.irc.connect((server, port))

        # Perform user authentication
        self.irc.send(bytes(
            "USER {} {} {} :user\n".format(nick, nick, nick), "UTF-8"
        ))
        self.irc.send(bytes(
            "NICK {}\n".format(nick), "UTF-8"
        ))
        self.irc.send(bytes(
            "NICKSERV IDENTIFY {}\n".format(password), "UTF-8"
        ))

        time.sleep(5)

        # join the channel
        self.irc.send(bytes(
            "JOIN {}\n".format(channel), "UTF-8"
        ))

    def get_response(self):

        time.sleep(1)

        # Get the response
        response = self.irc.recv(2040).decode("UTF-8")

        if response.find("PING") != -1:
            self.irc.send(bytes(
                "PONG {}\r\n".format(
                    response.split()[1]
                ),
                "UTF-8"
            ))

        return response
