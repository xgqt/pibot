#!/usr/bin/env python3


# This file is part of pibot.

# pibot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pibot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pibot.  If not, see <https://www.gnu.org/licenses/>.


from pibot.ii import IRC

import argparse


parser = argparse.ArgumentParser(
    description="Python IRC bot",
    epilog="Copyright (c) 2020, XGQT (License: GNU GPL Version 3)"
)
parser.add_argument(
    "-s", "--server",
    type=str, default="chat.freenode.net",
    help="server to connect to"
)
parser.add_argument(
    "-p", "--port",
    type=int, default=6665,
    help="server port"
)
parser.add_argument(
    "-c", "--channel",
    type=str, default="##testchannel",
    help="channel to join"
)
parser.add_argument(
    "-n", "--nick",
    type=str, default="pib",
    help="nick of the bot"
)
parser.add_argument(
    "-P", "--password",
    type=str, default="",
    help="password to be used"
)
args = parser.parse_args()


def main():

    # IRC Config
    irc = IRC()
    irc.connect(
        args.server,
        args.port,
        args.channel,
        args.nick,
        args.password
    )

    while True:

        text = irc.get_response()
        print("[R] Response: {}".format(text))

        if "PRIVMSG" in text and args.channel in text and "hello" in text:
            irc.send(args.channel, "Hello!")


if __name__ == '__main__':
    main()
